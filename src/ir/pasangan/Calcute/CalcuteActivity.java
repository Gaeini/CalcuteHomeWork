package ir.pasangan.Calcute;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class CalcuteActivity extends Activity {

    String beforeResult = "";
    String strGetText   = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final TextView txtOldResult = (TextView) findViewById(R.id.txtOldResult);
        final TextView txtNewResult = (TextView) findViewById(R.id.txtNewResult);
        final Button btn0 = (Button) findViewById(R.id.btn0);
        final Button btn1 = (Button) findViewById(R.id.btn1);
        final Button btn2 = (Button) findViewById(R.id.btn2);
        final Button btn3 = (Button) findViewById(R.id.btn3);
        final Button btn4 = (Button) findViewById(R.id.btn4);
        final Button btn5 = (Button) findViewById(R.id.btn5);
        final Button btn6 = (Button) findViewById(R.id.btn6);
        final Button btn7 = (Button) findViewById(R.id.btn7);
        final Button btn8 = (Button) findViewById(R.id.btn8);
        final Button btn9 = (Button) findViewById(R.id.btn9);
        final Button btnPlus = (Button) findViewById(R.id.btnPlus);
        final Button btnMinus = (Button) findViewById(R.id.btnMinus);
        final Button btnDivide = (Button) findViewById(R.id.btnDivide);
        final Button btnMultiply = (Button) findViewById(R.id.btnMultiply);
        final Button btnEqual = (Button) findViewById(R.id.btnEqual);
        final Button btnReset = (Button) findViewById(R.id.btnReset);
        final Button btnPoint = (Button) findViewById(R.id.btnPoint);

        OnClickListener numbericBtn = new OnClickListener() {

            @Override
            public void onClick(View view) {
                Button numBtn = (Button) view;
                strGetText = numBtn.getText().toString();
                beforeResult = txtNewResult.getText().toString();
                if (strGetText.equals("."))
                    if ( !beforeResult.contains(".")) {
                        txtNewResult.setText(beforeResult + strGetText);
                        return;
                    } else {
                        return;
                    }

                if (beforeResult.length() < 10) {
                    if (beforeResult.equals("0")) {
                        txtNewResult.setText(strGetText);
                    }
                    else {
                        txtNewResult.setText(beforeResult + strGetText);
                    }
                }
            }

        };

        OnClickListener operatorBtn = new OnClickListener() {

            @Override
            public void onClick(View view) {
                Button opBtn = (Button) view;
                String strOpBtn = opBtn.getText().toString();

                if (strOpBtn.equals("+")) {
                    if ( !txtNewResult.getText().toString().contains("+")) {
                        txtOldResult.setText(txtNewResult.getText().toString() + strOpBtn);
                        txtNewResult.setText("0");
                    }
                }

                if (strOpBtn.equals("="))
                    if (txtOldResult.getText().toString().contains("+")) {
                        String strTxtOldResult = txtOldResult.getText().toString().substring(0, txtOldResult.getText().toString().length() - 1);

                        int intTxtOldResult = Integer.parseInt(strTxtOldResult);
                        int intTxtNewResult = Integer.parseInt(txtNewResult.getText().toString());
                        txtNewResult.setText(intTxtOldResult + intTxtNewResult + "");
                        txtOldResult.setText(txtNewResult.getText().toString());
                        txtNewResult.setText("0");
                    }

            }
        };

        btnReset.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                txtNewResult.setText("0");
                txtOldResult.setText("0");

            }
        });

        btn0.setOnClickListener(numbericBtn);
        btn1.setOnClickListener(numbericBtn);
        btn2.setOnClickListener(numbericBtn);
        btn3.setOnClickListener(numbericBtn);
        btn4.setOnClickListener(numbericBtn);
        btn5.setOnClickListener(numbericBtn);
        btn6.setOnClickListener(numbericBtn);
        btn7.setOnClickListener(numbericBtn);
        btn8.setOnClickListener(numbericBtn);
        btn9.setOnClickListener(numbericBtn);
        btnPoint.setOnClickListener(numbericBtn);

        btnPlus.setOnClickListener(operatorBtn);
        btnMinus.setOnClickListener(operatorBtn);
        btnDivide.setOnClickListener(operatorBtn);
        btnMultiply.setOnClickListener(operatorBtn);
        btnEqual.setOnClickListener(operatorBtn);

    }
}